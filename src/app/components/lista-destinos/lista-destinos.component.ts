import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
destinos: DestinoViaje[];

  constructor() {
    this.destinos = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre:string, imgUrl:string):boolean{
    this.destinos.push(new DestinoViaje(nombre, imgUrl))
    return false
  }

}
