export class DestinoViaje {
    nombre:string;
    imgUrl:string;

    constructor(  nombre:string, imgUrl:string){
        this.nombre = nombre;
        this.imgUrl = imgUrl;
    }
}